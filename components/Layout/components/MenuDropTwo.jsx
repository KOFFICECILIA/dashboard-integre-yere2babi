import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Link from 'next/link'

export default function MenuDropTwo() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
        <p className="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"  onClick={handleClick}>
          <i className="icon-menu7"></i>
        </p>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
            <a href="#" className="dropdown-item"><i className="icon-undo"></i> Supprimer</a>
        </MenuItem>
        <MenuItem onClick={handleClose}>
            <a href="#" className="dropdown-item"><i className="icon-history"></i> Archiver</a>
        </MenuItem>
      </Menu>
    </div>
  );
}