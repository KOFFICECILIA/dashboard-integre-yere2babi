import Link from 'next/link'
import React from 'react'
import ContactDrop from '../ContactDrop'
import ActivityDrop from '../ActivityDrop'

export default function TopBar(){
    return (
        <div>
            <div className="navbar navbar-expand-md navbar-dark navbar-static">
                <div className="navbar-brand">
                    <Link href="/" className="d-inline-block">
                        <img src="global_assets/images/yèrè2babi.png" className="text-shadow-dark" style={{height:'50px',width:'99px',marginLeft:'35px'}} alt="" />
                    </Link>
                </div>

                <div className="d-md-none">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                        <i className="icon-tree5"></i>
                    </button>
                    <button className="navbar-toggler sidebar-mobile-main-toggle" type="button">
                        <i className="icon-paragraph-justify3"></i>
                    </button>
                </div>

                <div className="collapse navbar-collapse" id="navbar-mobile">
                    

                    <span className="navbar-text ml-md-3">
                        <span className="badge badge-mark mr-2" style={{border: "3px solid #FFB74D",padding:'3px', borderRadius:'100%'}}> </span>
                        bonjour, Tano !
                    </span>

                    <ul className="navbar-nav ml-md-auto">
                        <li className="nav-item dropdown">
                            <ContactDrop/>
                        </li>

                        <li className="nav-item dropdown">
                            <ActivityDrop/>
                        </li>

                        <li className="nav-item" style={{marginTop:'23px'}}>
                            <a href="#" className="navbar-nav-link">
                                <i className="icon-switch2"></i>
                                <span className="d-md-none ml-2">Déconnexion</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}