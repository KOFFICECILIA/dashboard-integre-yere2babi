import Link from 'next/link'
import React from 'react'
import MenuDropCompt from '../MenuDropCompt'
import Drawer from '@material-ui/core/Drawer';


export default function SideBar() {
   
    return (
        <div className="">
            <div className="sidebar sidebar-light sidebar-main sidebar-expand-md">

                <div className="sidebar-mobile-toggler text-center">
                    <a href="#" className="sidebar-mobile-main-toggle">
                        <i className="icon-arrow-left8"></i>
                    </a>
                    <span className="font-weight-semibold">Navigation</span>
                    <a href="#" className="sidebar-mobile-expand">
                        <i className="icon-screen-full"></i>
                        <i className="icon-screen-normal"></i>
                    </a>
                </div>

                <div className="sidebar-content">
                    <div className="sidebar-user-material">
                        <div className="sidebar-user-material-body">
                            <div className="card-body text-center">
                                <a href="#">
                                    <img src="global_assets/images/placeholders/placeholder.jpg" className="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="" />
                                </a>
                                <h6 className="mb-0 text-white text-shadow-dark">Tano Tano Thierry</h6>
                                <span className="font-size-sm text-white text-shadow-dark"> Cocody abidjan , CI</span>
                            </div>
                            <MenuDropCompt/>
                        </div>
                    </div>

                <div className="card card-sidebar-mobile">
                    <ul className="nav nav-sidebar" data-nav-type="accordion">
                        <li className="nav-item-header"><div className="text-uppercase font-size-xs line-height-xs"></div> <i className="icon-menu" title="Main"></i></li>
                        <li className="nav-item">
                            <Link href="/">
                                <a href="index.html" className="nav-link active">
                                    <i className="icon-home4"></i>
                                    <span>
                                        Tableau de bord
                                    </span>
                                </a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/listeAnonceurs">
                                <a href="listeAnonceurs.html" className="nav-link"><i className="icon-copy"></i> <span>Liste des anonceurs</span></a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/ajout_annonceurs">
                                <a href="ajout_anonceurs.html" className="nav-link"><i className="icon-user-plus"></i> <span>ajouter des anonceurs</span></a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/modifier_annonceur">
                                <a href="modififier.html" className="nav-link"><i className="icon-pencil3"></i> <span>Modifier des anonceurs</span></a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/acces_annonceurs">
                                <a href="acces_anonceurs.html" className="nav-link"><i className="icon-select2"></i> <span>Les accès</span></a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/detail_annonceurs">
                                <a href="details_annonceurs.html" className="nav-link"><i className="icon-insert-template"></i> <span>Détails Anonceurs</span></a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/liste_utilisateurs">
                                <a href="liste_utilisateurs.html" className="nav-link"><i className="icon-people"></i> <span>Liste des utilisateurs</span></a>
                            </Link>
                        </li>

                    </ul>
                </div>
            </div>
            </div>
        </div>
    )
}