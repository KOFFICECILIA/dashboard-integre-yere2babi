import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Link from 'next/link'

export default function MenuDropOne() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <p aria-controls="simple-menu" aria-haspopup="true" className="list-icons-item dropdown-toggle caret-0" onClick={handleClick}>
      {/* <a href="#" className="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"></a> */}
      <i className="icon-menu7"></i>
      </p>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
            <Link href="/detail_annonceurs">
                <a href="details_annonceurs.html" className="dropdown-item"><i className="icon-file-stats"></i> Voir</a>
            </Link>
        </MenuItem>
        <MenuItem onClick={handleClose}>
            <Link href="/modifier_annonceur">
                <a href="modifier.html" className="dropdown-item"><i className="icon-file-text2"></i> Modifier</a>
            </Link>
        </MenuItem>
        <MenuItem onClick={handleClose}>
            <Link href="/acces_annonceurs">
                <a href="acces_anonceurs.html" className="dropdown-item"><i className="icon-file-locked"></i> Désactiver</a>
            </Link>
        </MenuItem>
      </Menu>
    </div>
  );
}