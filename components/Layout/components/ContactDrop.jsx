import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Popper from '@material-ui/core/Popper';
import PopupState, { bindToggle, bindPopper } from 'material-ui-popup-state';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
}));

export default function PopperPopupState() {
  const classes = useStyles();

  return (
    <PopupState variant="popper" popupId="demo-popup-popper">
      {(popupState) => (
        <div style={{marginTop:'23px'}}>
          <p variant="contained"  className="navbar-nav-link dropdown-toggle my-drop" {...bindToggle(popupState)}>
          <i className="icon-make-group mr-2"></i> Contact
          </p>
          <Popper {...bindPopper(popupState)} transition>
            {({ TransitionProps }) => (
              <Fade {...TransitionProps} timeout={350}>
                <Paper>
                  <div className="container-fluid">
                    <div className="row no-gutters">
                        <div className="col-12 col-sm-4">
                            <a href="#" className="d-block text-default text-center ripple-dark rounded p-3">
                                <i className="icon-github4 icon-2x"></i>
                                <div className="font-size-sm font-weight-semibold text-uppercase mt-2">Github</div>
                            </a>

                            <a href="#" className="d-block text-default text-center ripple-dark rounded p-3">
                                <i className="icon-dropbox text-blue-400 icon-2x"></i>
                                <div className="font-size-sm font-weight-semibold text-uppercase mt-2">Dropbox</div>
                            </a>
                        </div>
                        <div className="col-12 col-sm-4">
                            <a href="#" className="d-block text-default text-center ripple-dark rounded p-3">
                                <i className="icon-dribbble3 text-pink-400 icon-2x"></i>
                                <div className="font-size-sm font-weight-semibold text-uppercase mt-2">Dribbble</div>
                            </a>

                            <a href="#" className="d-block text-default text-center ripple-dark rounded p-3">
                                <i className="icon-google-drive text-success-400 icon-2x"></i>
                                <div className="font-size-sm font-weight-semibold text-uppercase mt-2">Drive</div>
                            </a>
                        </div>
                        <div className="col-12 col-sm-4">
                            <a href="#" className="d-block text-default text-center ripple-dark rounded p-3">
                                <i className="icon-twitter text-info-400 icon-2x"></i>
                                <div className="font-size-sm font-weight-semibold text-uppercase mt-2">Twitter</div>
                            </a>

                            <a href="#" className="d-block text-default text-center ripple-dark rounded p-3">
                                <i className="icon-youtube text-danger icon-2x"></i>
                                <div className="font-size-sm font-weight-semibold text-uppercase mt-2">Youtube</div>
                            </a>
                        </div>
                    </div>
                  </div>
                </Paper>
              </Fade>
            )}
          </Popper>
        </div>
      )}
    </PopupState>
  );
}

