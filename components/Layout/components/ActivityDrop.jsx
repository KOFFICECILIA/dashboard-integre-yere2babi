import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Popper from '@material-ui/core/Popper';
import PopupState, { bindToggle, bindPopper } from 'material-ui-popup-state';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
}));

export default function PopperPopupState() {
  const classes = useStyles();

  return (
    <PopupState variant="popper" popupId="demo-popup-popper">
      {(popupState) => (
        <div style={{marginTop:'23px'}}>
          <p variant="contained"  className="navbar-nav-link dropdown-toggle my-drop" {...bindToggle(popupState)}>
            <i className="icon-pulse2 mr-2"></i> Activités
          </p>
          <Popper {...bindPopper(popupState)} transition style={{maxWidth:'350px'}}>
            {({ TransitionProps }) => (
              <Fade {...TransitionProps} timeout={350}>
                <Paper>
                  <div className="dropdown-menu-right dropdown-content wmin-md-350">
                    <div className="dropdown-content-header">
                        <span className="font-size-sm line-height-sm text-uppercase font-weight-semibold">notifications</span>
                        <a href="#" className="text-default"><i className="icon-search4 font-size-base"></i></a>
                    </div>
                    <div className="dropdown-content-body dropdown-scrollable">
                        <ul className="media-list">
                            <li className="media">
                                <div className="mr-3">
                                    <a href="#" className="btn bg-success-400 rounded-round btn-icon" style={{background:"#66BB6A", padding:'8px'}}><i className="icon-mention"></i></a>
                                </div>

                                <div className="media-body">
                                    <a href="#">Taylor Swift</a> mentioned you in a post "Angular JS. Tips and tricks"
                                    <div className="font-size-sm text-muted mt-1">4 minutes ago</div>
                                </div>
                            </li>

                            <li className="media">
                                <div className="mr-3">
                                    <a href="#" className="btn bg-pink-400 rounded-round btn-icon" style={{background:"#EC407A", padding:'8px'}}><i className="icon-paperplane"></i></a>
                                </div>
                                
                                <div className="media-body">
                                    Special offers have been sent to subscribed users by <a href="#">Donna Gordon</a>
                                    <div className="font-size-sm text-muted mt-1">36 minutes ago</div>
                                </div>
                            </li>

                            <li className="media">
                                <div className="mr-3">
                                    <a href="#" className="btn bg-blue rounded-round btn-icon" style={{background:"#03A9F4", padding:'8px'}}><i className="icon-plus3"></i></a>
                                </div>
                                
                                <div className="media-body">
                                    <a href="#">Chris Arney</a> created a new <span className="font-weight-semibold">Design</span> branch in <span className="font-weight-semibold">Limitless</span> repository
                                    <div className="font-size-sm text-muted mt-1">2 hours ago</div>
                                </div>
                            </li>

                            <li className="media">
                                <div className="mr-3">
                                    <a href="#" className="btn bg-purple-300 rounded-round btn-icon" style={{background:"#9575CD", padding:'8px'}}><i className="icon-truck"></i></a>
                                </div>
                                
                                <div className="media-body">
                                    Shipping cost to the Netherlands has been reduced, database updated
                                    <div className="font-size-sm text-muted mt-1">Feb 8, 11:30</div>
                                </div>
                            </li>

                            <li className="media">
                                <div className="mr-3">
                                    <a href="#" className="btn bg-warning-400 rounded-round btn-icon" style={{background:"#FF7043", padding:'8px'}}><i className="icon-comment"></i></a>
                                </div>
                                
                                <div className="media-body">
                                    New review received on <a href="#">Server side integration</a> services
                                    <div className="font-size-sm text-muted mt-1">Feb 2, 10:20</div>
                                </div>
                            </li>

                            <li className="media">
                                <div className="mr-3">
                                    <a href="#" className="btn bg-teal-400 rounded-round btn-icon" style={{background:"#26A69A", padding:'8px'}}><i className="icon-spinner11"></i></a>
                                </div>
                                
                                <div className="media-body">
                                    <strong>January, 2018</strong> - 1320 new users, 3284 orders, $49,390 revenue
                                    <div className="font-size-sm text-muted mt-1">Feb 1, 05:46</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                  </div>
                </Paper>
              </Fade>
            )}
          </Popper>
        </div>
      )}
    </PopupState>
  );
}

