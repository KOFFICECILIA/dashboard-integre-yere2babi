import React from 'react'

export default function Header() {
    return(
        <div className="content-wrapper">
            <div className="page-header page-header-light">
				<div className="page-header-content header-elements-md-inline">
					<div className="page-title d-flex">
						<h4><i className="icon-arrow-left52 mr-2"></i> <span className="font-weight-semibold">Accueil</span> - Tableau de bord</h4>
						<a href="#" className="header-elements-toggle text-default d-md-none"><i className="icon-more"></i></a>
					</div>
				</div>

				<div className="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div className="d-flex">
						<div className="breadcrumb">
							<a href="index.html" className="breadcrumb-item"><i className="icon-home2 mr-2"></i> Accueil</a>
							<span className="breadcrumb-item active">Tableau de bord</span>
						</div>

						<a href="#" className="header-elements-toggle text-default d-md-none"><i className="icon-more"></i></a>
					</div>
				</div>
			</div>
        </div>
    )
} 