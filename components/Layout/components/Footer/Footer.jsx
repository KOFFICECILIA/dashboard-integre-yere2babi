import Link from 'next/link'
import React from 'react'

export default function Footer() {
    return(
        <div style={{ background:'#fff',border:'1px solid #ccc', padding:'4px 7px'}}>
            <div className="">
				<div className="" style={{display:"flex", justifyContent:"space-between"}}>
					<span className="navbar-text">
						&copy; 2021 <a href="#"></a>yêrê de babi by <a href="http://www.weenovit.com" target="_blank">weenovit.com</a>
					</span>

					<ul className="navbar-nav ml-lg-auto">
						<li className="nav-item">
                            <Link href="/">
                                <a href="https://www.yèrè2babi.com" className="navbar-nav-link font-weight-semibold"><span className="text-orange-400"><i className="icon-server mr-2"></i> Yèrè2Babi</span></a>
                            </Link>
                        </li>
					</ul>
				</div>
			</div>
        </div>
    )
}