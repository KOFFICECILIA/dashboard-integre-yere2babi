import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500,
  },
}));

export default function MessagesTabs() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div className={classes.root} style={{width:'100%'}}>
      <AppBar position="static" color="default">
        <Tabs
            className="nav nav-tabs nav-tabs-solid nav-justified bg-indigo-400 border-x-0 border-bottom-0 border-top-indigo-300 mb-0"
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="#fff"
        >
          <Tab label="Mardi" {...a11yProps(0)} className="nav-item" />
          <Tab label="Lundi" {...a11yProps(1)} className="nav-item"/>
          <Tab label="Vendredi" {...a11yProps(2)} className="nav-item"/>
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
        className="tab-content card-body"
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
        <ul className="media-list">
            <li className="media">
              <div className="mr-3 position-relative">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
                <span className="badge bg-danger-400 badge-pill badge-float border-2 border-white">8</span>
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">James Alexander</a>
                  <span className="font-size-sm text-muted">14:58</span>
                </div>

                The constitutionally inventoried precariously...
              </div>
            </li>

            <li className="media">
              <div className="mr-3 position-relative">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
                <span className="badge bg-danger-400 badge-pill badge-float border-2 border-white">6</span>
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Margo Baker</a>
                  <span className="font-size-sm text-muted">12:16</span>
                </div>

                Pinched a well more moral chose goodness...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Jeremy Victorino</a>
                  <span className="font-size-sm text-muted">09:48</span>
                </div>

                Pert thickly mischievous clung frowned well...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Beatrix Diaz</a>
                  <span className="font-size-sm text-muted">05:54</span>
                </div>

                Nightingale taped hello bucolic fussily cardinal...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">												
                <div className="d-flex justify-content-between">
                  <a href="#">Richard Vango</a>
                  <span className="font-size-sm text-muted">01:43</span>
                </div>

                Amidst roadrunner distantly pompously where...
              </div>
            </li>
          </ul>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
        <ul className="media-list">
            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Isak Temes</a>
                  <span className="font-size-sm text-muted">Tue, 19:58</span>
                </div>

                Reasonable palpably rankly expressly grimy...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Vittorio Cosgrove</a>
                  <span className="font-size-sm text-muted">Tue, 16:35</span>
                </div>

                Arguably therefore more unexplainable fumed...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Hilary Talaugon</a>
                  <span className="font-size-sm text-muted">Tue, 12:16</span>
                </div>

                Nicely unlike porpoise a kookaburra past more...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Bobbie Seber</a>
                  <span className="font-size-sm text-muted">Tue, 09:20</span>
                </div>

                Before visual vigilantly fortuitous tortoise...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Walther Laws</a>
                  <span className="font-size-sm text-muted">Tue, 03:29</span>
                </div>

                Far affecting more leered unerringly dishonest...
              </div>
            </li>
          </ul>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
        <ul className="media-list">
            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Owen Stretch</a>
                  <span className="font-size-sm text-muted">Mon, 18:12</span>
                </div>

                Tardy rattlesnake seal raptly earthworm...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Jenilee Mcnair</a>
                  <span className="font-size-sm text-muted">Mon, 14:03</span>
                </div>

                Since hello dear pushed amid darn trite...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Alaster Jain</a>
                  <span className="font-size-sm text-muted">Mon, 13:59</span>
                </div>

                Dachshund cardinal dear next jeepers well...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Sigfrid Thisted</a>
                  <span className="font-size-sm text-muted">Mon, 09:26</span>
                </div>

                Lighted wolf yikes less lemur crud grunted...
              </div>
            </li>

            <li className="media">
              <div className="mr-3">
                <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="36" height="36" alt="" />
              </div>

              <div className="media-body">
                <div className="d-flex justify-content-between">
                  <a href="#">Sherilyn Mckee</a>
                  <span className="font-size-sm text-muted">Mon, 06:38</span>
                </div>

                Less unicorn a however careless husky...
              </div>
            </li>
          </ul>
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}
