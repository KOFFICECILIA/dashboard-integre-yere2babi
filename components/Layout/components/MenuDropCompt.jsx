import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function SimpleAccordion() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Accordion style={{borderRadius:'0', backgroundColor:'transparent', color:'#fff'}} className="sidebar-user-material-footer">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          style={{color:'#fff'}}
          className="d-flex justify-content-between align-items-center text-shadow-dark"
        >
          <Typography className={classes.heading}>Mon Compte</Typography>
        </AccordionSummary>
        <AccordionDetails style={{backgroundColor:'#fff'}}>
          <div>
                <ul className="nav nav-sidebar">
                    <li className="nav-item">
                        <a href="#" className="nav-link">
                            <i className="icon-comment-discussion"></i>
                            <span>Messages</span>
                            <span className="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link">
                            <i className="icon-cog5"></i>
                            <span>Paramètres</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link">
                            <i className="icon-switch2"></i>
                            <span>Déconnexion</span>
                        </a>
                    </li>
                </ul>
            </div>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
