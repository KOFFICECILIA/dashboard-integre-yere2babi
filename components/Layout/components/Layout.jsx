import React from "react"
import SideBar from "./SideBar/SideBar"
import TopBar from './TopBar/TopBar'
import Header from './Header/Header'
import Footer from './Footer/Footer'

export default function Layout({children}) {
    return (
        <>
            <TopBar />
            <div className="page-content">
                <SideBar />
                <div className="content-wrapper">
                    <Header />
                    {children}
                    <Footer />
                </div>
            </div>
            
            
        </>
    )
}