import { container } from "../../../nextjs-material-kit";

const carouselStyle = {
  section: {
    padding: "0"
  },
  container,
  marginAuto: {
    width: "100%",
    marginLeft: "0 !important",
    marginRight: "0 !important"
  }
};

export default carouselStyle;
