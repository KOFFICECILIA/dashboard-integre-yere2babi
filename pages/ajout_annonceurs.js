import React from 'react'

export default function Ajout_annonceurs(){
    return(
        <div className="content">

				<div className="container">
					<div className="row">
						<div className="col-md-12">
	
							<div className="mb-3">
								<h6 className="mb-0 font-weight-semibold">
									Inscription anonceurs
								</h6>
								<span className="text-muted d-block">Remplissez tout les champs ! </span>
							</div>
	
							<div className="card">
								<div className="card-header header-elements-inline">
										<h5 className="card-title"><i className="icon-reading"></i> ajouter</h5>
									<div className="header-elements">
										<div className="list-icons">
											<a className="list-icons-item" data-action="collapse"></a>
											<a className="list-icons-item" data-action="reload"></a>
											<a className="list-icons-item" data-action="remove"></a>
										</div>
									</div>
								</div>
	
								<div className="card-body">
									<div className="form-group form-group-float">
										<label className="form-group-float-label">Nom et Prénom</label>
										<input type="text" className="form-control" placeholder="Nom et Prénom" />
									</div>
	
									<div className="form-group form-group-float">
										<label className="form-group-float-label">Nom de votre boutique</label>
										<input type="text" className="form-control" placeholder="Nom de votre boutique" />
									</div>
	
									<div className="form-group form-group-float">
										<label className="form-group-float-label">Password input</label>
										<input type="password" className="form-control" placeholder="Password input" />
									</div>
	
									<div className="form-group form-group-float">
										<label className="form-group-float-label text-success font-weight-semibold">email</label>
										<div className="form-group-feedback form-group-feedback-right">
											<input type="text" className="form-control border-success" placeholder="Votre email" />
											<div className="form-control-feedback text-success">
												<i className="icon-checkmark3"></i>
											</div>
										</div>
										<span className="form-text text-success">Contextual input helper</span>
									</div>
	
									<div className="form-group form-group-float">
										<label className="form-group-float-label">Localisation</label>
										<select data-placeholder="Localisation" className="form-control form-control-select2" data-fouc>
											<option></option>
											<option value="AK">Abidjan</option>
											<option value="HI">Adzopé</option>
											<option value="CA">Akoupé</option>
											<option value="NV">Bongouanou</option>
											<option value="WA">Bouna</option>
											<option value="AZ">Bondoukou</option>
											<option value="CO">Daloa</option>
											<option value="WY">Divo</option>
										</select>
									</div>
	
									<div className="form-group form-group-float">
										<label className="d-block form-group-float-label">Votre logo</label>
										<input type="file" className="form-control-uniform" data-fouc />
									</div>
	
									<div className="form-group form-group-float">
										<label className="form-group-float-label">Description</label>
										<textarea rows="5" cols="5" className="form-control" placeholder="Description"></textarea>
									</div>
									<button type="button" className="btn bg-teal mr-3">Envoyer</button>
									<button type="button" className="btn bg-danger">Annuler</button>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
    )
}