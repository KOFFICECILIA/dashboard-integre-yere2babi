import React from 'react'
import '../styles/globals.css'
import '../public/assets/css/bootstrap.min.css'
import Layout from '../components/Layout/components/Layout'

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;
