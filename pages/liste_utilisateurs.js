import React from 'react'

export default function Liste_utilisateurs(){
    return(
        <div className="container">
            <div className="card mt-4">
                <div className="card-header header-elements-inline">
                    <h5 className="card-title">Liste des Utilisateurs</h5>
                    <div className="header-elements">
                        <div className="list-icons">
                            <a className="list-icons-item" data-action="collapse"></a>
                            <a className="list-icons-item" data-action="reload"></a>
                            <a className="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>

                <table className="table datatable-basic">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Communes</th>
                            <th>Dernière connexion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Marth</td>
                            <td><a href="#">Enright</a></td>
                            <td>Abobo</td>
                            <td>22 Jun 1972</td>
                        </tr>
                        <tr>
                            <td>Jackelyn</td>
                            <td>Weible</td>
                            <td><a href="#">Daloa</a></td>
                            <td>3 Oct 1981</td>
                        </tr>
                        <tr>
                            <td>Aura</td>
                            <td>Hard</td>
                            <td>Cocody</td>
                            <td>19 Apr 1969</td>
                        </tr>
                        <tr>
                            <td>Nathalie</td>
                            <td><a href="#">Pretty</a></td>
                            <td>Bingerville</td>
                            <td>13 Dec 1977</td>
                        </tr>
                        <tr>
                            <td>Sharan</td>
                            <td>Leland</td>
                            <td>Adjamé</td>
                            <td>30 Dec 1991</td>
                        </tr>
                        <tr>
                            <td>Maxine</td>
                            <td><a href="#">Woldt</a></td>
                            <td><a href="#">Yopougon</a></td>
                            <td>17 Oct 1987</td>
                        </tr>
                        <tr>
                            <td>Sylvia</td>
                            <td><a href="#">Mcgaughy</a></td>
                            <td>Plateau</td>
                            <td>11 Nov 1983</td>
                        </tr>
                        <tr>
                            <td>Lizzee</td>
                            <td><a href="#">Goodlow</a></td>
                            <td>Bondoukou</td>
                            <td>1 Nov 1961</td>
                        </tr>
                        <tr>
                            <td>Kennedy</td>
                            <td>Haley</td>
                            <td>Abobo</td>
                            <td>18 Dec 1960</td>
                        </tr>
                        <tr>
                            <td>Chantal</td>
                            <td><a href="#">Nailor</a></td>
                            <td>Cocody</td>
                            <td>10 Jan 1980</td>
                        </tr>
                        <tr>
                            <td>Delma</td>
                            <td>Bonds</td>
                            <td>Yopougnon</td>
                            <td>21 Dec 1968</td>
                        </tr>
                        <tr>
                            <td>Roland</td>
                            <td>Salmos</td>
                            <td><a href="#">Senior Program Developer</a></td>
                            <td>5 Jun 1986</td>
                        </tr>
                        <tr>
                            <td>Coy</td>
                            <td>Wollard</td>
                            <td>Adjamé</td>
                            <td>12 Oct 1982</td>
                        </tr>
                        <tr>
                            <td>Maxwell</td>
                            <td>Maben</td>
                            <td>Abobo</td>
                            <td>25 Feb 1988</td>
                        </tr>
                        <tr>
                            <td>Cicely</td>
                            <td>Sigler</td>
                            <td><a href="#">Bingerville</a></td>
                            <td>15 Mar 1960</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}