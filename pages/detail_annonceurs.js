import React from 'react'
import Graph from '../components/Layout/components/Graph'

export default function Detail_annonceurs() {
    return(
        <div>
            <div className="profile-cover">
					<div className="profile-cover-img" style={{backgroundImage: "url(../global_assets/images/cover.jpg)"}}></div>
					<div className="media align-items-center text-center text-md-left flex-column flex-md-row m-0">
						<div className="mr-md-3 mb-2 mb-md-0">
							<a href="#" className="profile-thumb">
								<img src="../global_assets/images/user3.jpg" className="border-white rounded-circle" width="48" height="48" alt="" />
							</a>
						</div>

						<div className="media-body text-white">
				    		<h1 className="mb-0">KMCAD-boutique</h1>
				    		<span className="d-block">UX/UI designer</span>
						</div>

						<div className="ml-md-3 mt-2 mt-md-0">
							<ul className="list-inline list-inline-condensed mb-0">
								<li className="list-inline-item"><a href="#" className="btn btn-light border-transparent"><i className="icon-file-picture mr-2"></i> Cover image</a></li>
								<li className="list-inline-item"><a href="#" className="btn btn-light border-transparent"><i className="icon-file-stats mr-2"></i> Statistiques</a></li>
							</ul>
						</div>
					</div>
				</div>


				<div className="navbar navbar-expand-lg navbar-light bg-light">
					<div className="text-center d-lg-none w-100">
						<button type="button" className="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-second">
							<i className="icon-menu7 mr-2"></i>
							Profile navigation
						</button>
					</div>

					<div className="navbar-collapse collapse" id="navbar-second">
						<ul className="nav navbar-nav">
							<li className="nav-item">
								<a href="#activity" className="navbar-nav-link active" data-toggle="tab">
									<i className="icon-menu7 mr-2"></i>
									Activités
								</a>
							</li>
							<li className="nav-item">
								<a href="#schedule" className="navbar-nav-link" data-toggle="tab">
									<i className="icon-calendar3 mr-2"></i>
									Chiffres
									<span className="badge badge-pill bg-success position-static ml-auto ml-lg-2">32</span>
								</a>
							</li>
							<li className="nav-item">
								<a href="#settings" className="navbar-nav-link" data-toggle="tab">
									<i className="icon-cog3 mr-2"></i>
									Paramètres
								</a>
							</li>
						</ul>

						<ul className="navbar-nav ml-lg-auto">
							<li className="nav-item">
								<a href="#" className="navbar-nav-link">
									<i className="icon-images3 mr-2"></i>
									Photos
								</a>
							</li>
							<li className="nav-item">
								<a href="#" className="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
									<i className="icon-gear"></i>
									<span className="d-lg-none ml-2">paramétre</span>
								</a>

								<div className="dropdown-menu dropdown-menu-right">
									<a href="#" className="dropdown-item"><i className="icon-image2"></i> Update cover</a>
									<a href="#" className="dropdown-item"><i className="icon-clippy"></i> Update info</a>
									<a href="#" className="dropdown-item"><i className="icon-make-group"></i> Manage sections</a>
									<div className="dropdown-divider"></div>
									<a href="#" className="dropdown-item"><i className="icon-three-bars"></i> Activity log</a>
									<a href="#" className="dropdown-item"><i className="icon-cog5"></i> Profile settings</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
				


		
			<div className="content">

				
				<div className="d-flex align-items-start flex-column flex-md-row">

					
					<div className="tab-content w-100 order-2 order-md-1">
						<div className="tab-pane fade active show" id="activity">

							
							<div className="card">
								<div className="card-header header-elements-sm-inline">
									<h6 className="card-title">Weekly statistics</h6>
									<div className="header-elements">
										<span><i className="icon-history mr-2 text-success"></i> Updated 3 hours ago</span>

										<div className="list-icons ml-3">
					                		<a className="list-icons-item" data-action="reload"></a>
					                	</div>
				                	</div>
								</div>

								<div className="card-body">
									<div className="chart-container">
										<div className="chart" id="weekly_statistics">
										<Graph />
										</div>
									</div>
								</div>
							</div>


							<div className="card">
								<div className="card-header header-elements-sm-inline">
									<h6 className="card-title">Himalayan sunset</h6>
									<div className="header-elements">
										<span><i className="icon-checkmark-circle mr-2 text-success"></i> 49 minutes ago</span>
										<div className="list-icons ml-3">
											<div className="list-icons-item dropdown">
												<a href="#" className="list-icons-item caret-0 dropdown-toggle" data-toggle="dropdown">
													<i className="icon-arrow-down12"></i>
												</a>

												<div className="dropdown-menu dropdown-menu-right">
													<a href="#" className="dropdown-item"><i className="icon-user-lock"></i> Hide user posts</a>
													<a href="#" className="dropdown-item"><i className="icon-user-block"></i> Block user</a>
													<a href="#" className="dropdown-item"><i className="icon-user-minus"></i> Unfollow user</a>
													<div className="dropdown-divider"></div>
													<a href="#" className="dropdown-item"><i className="icon-embed"></i> Embed post</a>
													<a href="#" className="dropdown-item"><i className="icon-blocked"></i> Report this post</a>
												</div>
											</div>
					                	</div>
				                	</div>
								</div>

								<div className="card-body">
									<div className="card-img-actions mb-3">
										<img className="card-img img-fluid" src="global_assets/images/placeholders/cover.jpg" alt="" />
										<div className="card-img-actions-overlay card-img">
											<a href="blog_single.html" className="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
												<i className="icon-link"></i>
											</a>
										</div>
									</div>

									<h6 className="mb-3">
										<i className="icon-comment-discussion mr-2"></i>
										<a href="#">Jason Ansley</a> commented:
									</h6>

									<blockquote className="blockquote blockquote-bordered py-2 pl-3 mb-0">
										<p className="mb-2 font-size-base">When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra despite taped laughed the much audacious less inside tiger groaned darn stuffily metaphoric unihibitedly inside cobra.</p>
										<footer className="blockquote-footer">Jason, <cite title="Source Title">10:39 am</cite></footer>
									</blockquote>
								</div>

								<div className="card-footer bg-transparent d-sm-flex justify-content-sm-between align-items-sm-center border-top-0 pt-0 pb-3">
									<ul className="list-inline mb-0">
										<li className="list-inline-item"><a href="#" className="text-default"><i className="icon-eye4 mr-2"></i> 438</a></li>
										<li className="list-inline-item"><a href="#" className="text-default"><i className="icon-comment-discussion mr-2"></i> 71</a></li>
									</ul>

									<a href="#" className="d-inline-block text-default mt-2 mt-sm-0">Read post <i className="icon-arrow-right14 ml-2"></i></a>
								</div>
							</div>


						
							<div className="row">
								<div className="col-lg-6">
									<div className="card border-left-3 border-left-danger rounded-left-0">
										<div className="card-body">
											<div className="d-sm-flex align-item-sm-center flex-sm-nowrap">
												<div>
													<h6 className="font-weight-semibold">Leonardo Fellini</h6>
													<ul className="list list-unstyled mb-0">
														<li>Invoice #: &nbsp;0028</li>
														<li>Issued on: <span className="font-weight-semibold">2015/01/25</span></li>
													</ul>
												</div>

												<div className="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
													<h6 className="font-weight-semibold">$8,750</h6>
													<ul className="list list-unstyled mb-0">
														<li>Method: <span className="font-weight-semibold">SWIFT</span></li>
														<li className="dropdown">
															Status: &nbsp;
															<a href="#" className="badge bg-danger-400 align-top dropdown-toggle" data-toggle="dropdown">Overdue</a>
															<div className="dropdown-menu dropdown-menu-right">
																<a href="#" className="dropdown-item active"><i className="icon-alert"></i> Overdue</a>
																<a href="#" className="dropdown-item"><i className="icon-alarm"></i> Pending</a>
																<a href="#" className="dropdown-item"><i className="icon-checkmark3"></i> Paid</a>
																<div className="dropdown-divider"></div>
																<a href="#" className="dropdown-item"><i className="icon-spinner2 spinner"></i> On hold</a>
																<a href="#" className="dropdown-item"><i className="icon-cross2"></i> Canceled</a>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>

										<div className="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
											<span>
												<span className="badge badge-mark border-danger mr-2"></span>
												Due:
												<span className="font-weight-semibold">2015/02/25</span>
											</span>

											<ul className="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
												<li className="list-inline-item">
													<a href="#" className="text-default"><i className="icon-eye8"></i></a>
												</li>
												<li className="list-inline-item dropdown">
													<a href="#" className="text-default dropdown-toggle" data-toggle="dropdown"><i className="icon-menu7"></i></a>

													<div className="dropdown-menu dropdown-menu-right">
														<a href="#" className="dropdown-item"><i className="icon-printer"></i> Print invoice</a>
														<a href="#" className="dropdown-item"><i className="icon-file-download"></i> Download invoice</a>
														<div className="dropdown-divider"></div>
														<a href="#" className="dropdown-item"><i className="icon-file-plus"></i> Edit invoice</a>
														<a href="#" className="dropdown-item"><i className="icon-cross2"></i> Remove invoice</a>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>

								<div className="col-lg-6">
									<div className="card border-left-3 border-left-success rounded-left-0">
										<div className="card-body">
											<div className="d-sm-flex align-item-sm-center flex-sm-nowrap">
												<div>
													<h6 className="font-weight-semibold">Rebecca Manes</h6>
													<ul className="list list-unstyled mb-0">
														<li>Invoice #: &nbsp;0027</li>
														<li>Issued on: <span className="font-weight-semibold">2015/02/24</span></li>
													</ul>
												</div>

												<div className="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
													<h6 className="font-weight-semibold">$5,100</h6>
													<ul className="list list-unstyled mb-0">
														<li>Method: <span className="font-weight-semibold">Paypal</span></li>
														<li className="dropdown">
															Status: &nbsp;
															<a href="#" className="badge bg-success-400 align-top dropdown-toggle" data-toggle="dropdown">Paid</a>
															<div className="dropdown-menu dropdown-menu-right">
																<a href="#" className="dropdown-item"><i className="icon-alert"></i> Overdue</a>
																<a href="#" className="dropdown-item"><i className="icon-alarm"></i> Pending</a>
																<a href="#" className="dropdown-item active"><i className="icon-checkmark3"></i> Paid</a>
																<div className="dropdown-divider"></div>
																<a href="#" className="dropdown-item"><i className="icon-spinner2 spinner"></i> On hold</a>
																<a href="#" className="dropdown-item"><i className="icon-cross2"></i> Canceled</a>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>

										<div className="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
											<span>
												<span className="badge badge-mark border-success mr-2"></span>
												Due:
												<span className="font-weight-semibold">2015/03/24</span>
											</span>

											<ul className="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
												<li className="list-inline-item">
													<a href="#" className="text-default"><i className="icon-eye8"></i></a>
												</li>
												<li className="list-inline-item dropdown">
													<a href="#" className="text-default dropdown-toggle" data-toggle="dropdown"><i className="icon-menu7"></i></a>

													<div className="dropdown-menu dropdown-menu-right">
														<a href="#" className="dropdown-item"><i className="icon-printer"></i> Print invoice</a>
														<a href="#" className="dropdown-item"><i className="icon-file-download"></i> Download invoice</a>
														<div className="dropdown-divider"></div>
														<a href="#" className="dropdown-item"><i className="icon-file-plus"></i> Edit invoice</a>
														<a href="#" className="dropdown-item"><i className="icon-cross2"></i> Remove invoice</a>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

					    </div>

					    <div className="tab-pane fade" id="schedule">

				    		
							<div className="card">
								<div className="card-header header-elements-inline">
									<h6 className="card-title">Available hours</h6>
									<div className="header-elements">
										<div className="list-icons">
					                		<a className="list-icons-item" data-action="collapse"></a>
					                		<a className="list-icons-item" data-action="reload"></a>
					                		<a className="list-icons-item" data-action="remove"></a>
					                	</div>
				                	</div>
								</div>

								<div className="card-body">
									<div className="chart-container">
										<div className="chart has-fixed-height" id="available_hours"></div>
									</div>
								</div>
							</div>


							<div className="card">
								<div className="card-header header-elements-inline">
									<h5 className="card-title">My schedule</h5>
									<div className="header-elements">
										<div className="list-icons">
					                		<a className="list-icons-item" data-action="collapse"></a>
					                		<a className="list-icons-item" data-action="reload"></a>
					                		<a className="list-icons-item" data-action="remove"></a>
					                	</div>
				                	</div>
								</div>

								<div className="card-body">
									<div className="my-schedule"></div>
								</div>
							</div>

				    	</div>

					    <div className="tab-pane fade" id="settings">

							{/* <!-- Profile info --> */}
							<div className="card">
								<div className="card-header header-elements-inline">
									<h5 className="card-title">Profile information</h5>
									<div className="header-elements">
										<div className="list-icons">
					                		<a className="list-icons-item" data-action="collapse"></a>
					                		<a className="list-icons-item" data-action="reload"></a>
					                		<a className="list-icons-item" data-action="remove"></a>
					                	</div>
				                	</div>
								</div>

								<div className="card-body">
									<form action="#">
										<div className="form-group">
											<div className="row">
												<div className="col-md-6">
													<label>Username</label>
													<input type="text" value="Eugene" className="form-control" />
												</div>
												<div className="col-md-6">
													<label>Full name</label>
													<input type="text" value="Kopyov" className="form-control" />
												</div>
											</div>
										</div>

										<div className="form-group">
											<div className="row">
												<div className="col-md-6">
													<label>Address line 1</label>
													<input type="text" value="Ring street 12" className="form-control" />
												</div>
												<div className="col-md-6">
													<label>Address line 2</label>
													<input type="text" value="building D, flat #67" className="form-control" />
												</div>
											</div>
										</div>

										<div className="form-group">
											<div className="row">
												<div className="col-md-4">
													<label>City</label>
													<input type="text" value="Munich" className="form-control" />
												</div>
												<div className="col-md-4">
													<label>State/Province</label>
													<input type="text" value="Bayern" className="form-control" />
												</div>
												<div className="col-md-4">
													<label>ZIP code</label>
													<input type="text" value="1031" className="form-control" />
												</div>
											</div>
										</div>

										<div className="form-group">
											<div className="row">
												<div className="col-md-6">
													<label>Email</label>
													<input type="text" readonly="readonly" value="eugene@kopyov.com" className="form-control" />
												</div>
												<div className="col-md-6">
						                            <label>Your country</label>
						                            <select className="form-control form-control-select2" data-fouc>
						                                <option value="germany" selected>Germany</option> 
						                                <option value="france">France</option> 
						                                <option value="spain">Spain</option> 
						                                <option value="netherlands">Netherlands</option> 
						                                <option value="other">...</option> 
						                                <option value="uk">United Kingdom</option> 
						                            </select>
												</div>
											</div>
										</div>

				                        <div className="form-group">
				                        	<div className="row">
				                        		<div className="col-md-6">
													<label>Phone #</label>
													<input type="text" value="+99-99-9999-9999" className="form-control" />
													<span className="form-text text-muted">+99-99-9999-9999</span>
				                        		</div>

												<div className="col-md-6">
													<label>Upload profile image</label>
				                                    <input type="file" className="form-input-styled" data-fouc />
				                                    <span className="form-text text-muted">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
												</div>
				                        	</div>
				                        </div>

				                        <div className="text-right">
				                        	<button type="submit" className="btn btn-primary">Save changes</button>
				                        </div>
									</form>
								</div>
							</div>


							
							<div className="card">
								<div className="card-header header-elements-inline">
									<h5 className="card-title">Account settings</h5>
									<div className="header-elements">
										<div className="list-icons">
					                		<a className="list-icons-item" data-action="collapse"></a>
					                		<a className="list-icons-item" data-action="reload"></a>
					                		<a className="list-icons-item" data-action="remove"></a>
					                	</div>
				                	</div>
								</div>

								<div className="card-body">
									<form action="#">
										<div className="form-group">
											<div className="row">
												<div className="col-md-6">
													<label>Username</label>
													<input type="text" value="Kopyov" readonly="readonly" className="form-control" />
												</div>

												<div className="col-md-6">
													<label>Current password</label>
													<input type="password" value="password" readonly="readonly" className="form-control" />
												</div>
											</div>
										</div>

										<div className="form-group">
											<div className="row">
												<div className="col-md-6">
													<label>New password</label>
													<input type="password" placeholder="Enter new password" className="form-control" />
												</div>

												<div className="col-md-6">
													<label>Repeat password</label>
													<input type="password" placeholder="Repeat new password" className="form-control" />
												</div>
											</div>
										</div>

										<div className="form-group">
											<div className="row">
												<div className="col-md-6">
													<label>Profile visibility</label>

													<div className="form-check">
														<label className="form-check-label">
															<input type="radio" name="visibility" className="form-input-styled" checked data-fouc />
															Visible to everyone
														</label>
													</div>

													<div className="form-check">
														<label className="form-check-label">
															<input type="radio" name="visibility" className="form-input-styled" data-fouc />
															Visible to friends only
														</label>
													</div>

													<div className="form-check">
														<label className="form-check-label">
															<input type="radio" name="visibility" className="form-input-styled" data-fouc />
															Visible to my connections only
														</label>
													</div>

													<div className="form-check">
														<label className="form-check-label">
															<input type="radio" name="visibility" className="form-input-styled" data-fouc />
															Visible to my colleagues only
														</label>
													</div>
												</div>

												<div className="col-md-6">
													<label>Notifications</label>

													<div className="form-check">
														<label className="form-check-label">
															<input type="checkbox" className="form-input-styled" checked data-fouc />
															Password expiration notification
														</label>
													</div>

													<div className="form-check">
														<label className="form-check-label">
															<input type="checkbox" className="form-input-styled" checked data-fouc />
															New message notification
														</label>
													</div>

													<div className="form-check">
														<label className="form-check-label">
															<input type="checkbox" className="form-input-styled" checked data-fouc />
															New task notification
														</label>
													</div>

													<div className="form-check">
														<label className="form-check-label">
															<input type="checkbox" className="form-input-styled" />
															New contact request notification
														</label>
													</div>
												</div>
											</div>
										</div>

				                        <div className="text-right">
				                        	<button type="submit" className="btn btn-primary">Save changes</button>
				                        </div>
			                        </form>
								</div>
							</div>

				    	</div>
					</div>
					

					<div className="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-300 border-0 shadow-0 order-1 order-lg-2 sidebar-expand-md">

						
						<div className="sidebar-content">

							
							<div className="card">
								<div className="card-header bg-transparent header-elements-inline">
									<span className="card-title font-weight-semibold">Share your thoughts</span>
									<div className="header-elements">
										<div className="list-icons">
					                		<a className="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div className="card-body">
									<form action="#">
				                    	<textarea name="enter-message" className="form-control mb-3" rows="3" cols="1" placeholder="Enter your message..."></textarea>

				                    	<div className="d-flex align-items-center">
				                    		<div className="list-icons list-icons-extended">
				                                <a href="#" className="list-icons-item" data-popup="tooltip" title="Add photo" data-container="body"><i className="icon-images2"></i></a>
				                            	<a href="#" className="list-icons-item" data-popup="tooltip" title="Add video" data-container="body"><i className="icon-film2"></i></a>
				                                <a href="#" className="list-icons-item" data-popup="tooltip" title="Add event" data-container="body"><i className="icon-calendar2"></i></a>
				                    		</div>

				                    		<button type="button" className="btn bg-blue btn-labeled btn-labeled-right ml-auto"><b><i className="icon-paperplane"></i></b> Share</button>
				                    	</div>
									</form>
								</div>
							</div>


							<div className="card">
								<div className="card-header bg-transparent header-elements-inline">
									<span className="card-title font-weight-semibold">Balance changes</span>
									<div className="header-elements">
										<span><i className="icon-arrow-down22 text-danger"></i> <span className="font-weight-semibold">- 29.4%</span></span>
			                		</div>
								</div>

								<div className="card-body">
									<div className="chart-container">
										<div className="chart has-fixed-height" id="balance_statistics"></div>
									</div>
								</div>
							</div>


							<div className="card">
								<div className="card-header bg-transparent header-elements-inline">
									<span className="card-title font-weight-semibold">Utilisateurs connectés</span>
									<div className="header-elements">
										<span className="badge bg-success badge-pill">+32</span>
			                		</div>
								</div>

								<ul className="media-list media-list-linked my-2">
									<li className="media font-weight-semibold border-0 py-2"></li>

									<li>
										<a href="#" className="media">
											<div className="mr-3">
												<img src="../global_assets/images/user3.jpg" className="rounded-circle" width="40" height="40" alt="" />
											</div>
											<div className="media-body">
												<div className="media-title font-weight-semibold">James Alexander</div>
												<span className="text-muted font-size-sm">UI/UX expert</span>
											</div>
											<div className="align-self-center ml-3">
												<span className="badge badge-mark bg-success border-success"></span>
											</div>
										</a>
									</li>

									<li>
										<a href="#" className="media">
											<div className="mr-3">
												<img src="../global_assets/images/user3.jpg" className="rounded-circle" width="40" height="40" alt="" />
											</div>
											<div className="media-body">
												<div className="media-title font-weight-semibold">Jeremy Victorino</div>
												<span className="text-muted font-size-sm">Senior designer</span>
											</div>
											<div className="align-self-center ml-3">
												<span className="badge badge-mark bg-danger border-danger"></span>
											</div>
										</a>
									</li>

									<li>
										<a href="#" className="media">
											<div className="mr-3">
												<img src="../global_assets/images/user3.jpg" className="rounded-circle" width="40" height="40" alt="" />
											</div>
											<div className="media-body">
												<div className="media-title"><span className="font-weight-semibold">Jordana Mills</span></div>
												<span className="text-muted">Sales consultant</span>
											</div>
											<div className="align-self-center ml-3">
												<span className="badge badge-mark bg-grey-300 border-grey-300"></span>
											</div>
										</a>
									</li>

									<li>
										<a href="#" className="media">
											<div className="mr-3">
												<img src="../global_assets/images/user3.jpg" className="rounded-circle" width="40" height="40" alt="" />
											</div>
											<div className="media-body">
												<div className="media-title"><span className="font-weight-semibold">William Miles</span></div>
												<span className="text-muted">SEO expert</span>
											</div>
											<div className="align-self-center ml-3">
												<span className="badge badge-mark bg-success border-success"></span>
											</div>
										</a>
									</li>

									<li className="media font-weight-semibold border-0 py-2">Partenaires</li>

									<li>
										<a href="#" className="media">
											<div className="mr-3">
												<img src="../global_assets/images/user3.jpg" className="rounded-circle" width="40" height="40" alt="" />
											</div>
											<div className="media-body">
												<div className="media-title font-weight-semibold">Margo Baker</div>
												<span className="text-muted font-size-sm">Google</span>
											</div>
											<div className="align-self-center ml-3">
												<span className="badge badge-mark bg-success border-success"></span>
											</div>
										</a>
									</li>

									<li>
										<a href="#" className="media">
											<div className="mr-3">
												<img src="../global_assets/images/user3.jpg" className="rounded-circle" width="40" height="40" alt="" />
											</div>
											<div className="media-body">
												<div className="media-title font-weight-semibold">Beatrix Diaz</div>
												<span className="text-muted font-size-sm">Facebook</span>
											</div>
											<div className="align-self-center ml-3">
												<span className="badge badge-mark bg-warning-400 border-warning-400"></span>
											</div>
										</a>
									</li>

									<li>
										<a href="#" className="media">
											<div className="mr-3">
												<img src="../global_assets/images/user3.jpg" className="rounded-circle" width="40" height="40" alt="" />
											</div>
											<div className="media-body">
												<div className="media-title font-weight-semibold">Richard Vango</div>
												<span className="text-muted font-size-sm">Microsoft</span>
											</div>
											<div className="align-self-center ml-3">
												<span className="badge badge-mark bg-grey-300 border-grey-300"></span>
											</div>
										</a>
									</li>
								</ul>
							</div>

						</div>

					</div>

				</div>

			</div>
        </div>
    )
}