import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useRouter } from 'next/router'
import MenuDropOne from '../components/Layout/components/MenuDropOne'
import MenuDropTwo from '../components/Layout/components/MenuDropTwo'
import MenuDropArch from '../components/Layout/components/MenuDropArch'
import MessagesTabs from '../components/Layout/components/MessagesTabs'
import Chart from '../components/Layout/components/Chart'
import Graph from '../components/Layout/components/Graph'



import { withStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const PurpleSwitch = withStyles({
  switchBase: {
    color: purple[300],
    '&$checked': {
      color: purple[500],
    },
    '&$checked + $track': {
      backgroundColor: purple[500],
    },
  },
  checked: {},
  track: {},
})(Switch);

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: '#52d869',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

const AntSwitch = withStyles((theme) => ({
  root: {
    width: 28,
    height: 16,
    padding: 0,
    display: 'flex',
  },
  switchBase: {
    padding: 2,
    color: theme.palette.grey[500],
    '&$checked': {
      transform: 'translateX(12px)',
      color: theme.palette.common.white,
      '& + $track': {
        opacity: 1,
        backgroundColor: theme.palette.primary.main,
        borderColor: theme.palette.primary.main,
      },
    },
  },
  thumb: {
    width: 12,
    height: 12,
    boxShadow: 'none',
  },
  track: {
    border: `1px solid ${theme.palette.grey[500]}`,
    borderRadius: 16 / 2,
    opacity: 1,
    backgroundColor: theme.palette.common.white,
  },
  checked: {},
}))(Switch);


export default function Home() {

  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedC: true,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  return (
    <div className="content">
        {/* <!-- Main charts --> */}
        <div className="row">
          <div className="col-xl-7">

            {/* <!-- Traffic sources --> */}
            <div className="card">
              <div className="card-header header-elements-inline">
                <h6 className="card-title">Traffic sources</h6>
                <div className="header-elements">
                  <div className="form-check form-check-right form-check-switchery form-check-switchery-sm">
                      <FormGroup>
                      <FormControlLabel
                        value="start"
                        control={<Switch color="primary" />}
                        label="Live update :"
                        backgroundColor='gray'
                        labelPlacement="start"
                      />
                      </FormGroup>
                  </div>
                </div>
              </div>

              <div className="card-body py-0">
                <div className="row">
                  <div className="col-sm-4">
                    <div className="d-flex align-items-center justify-content-center mb-2">
                      <a href="#" className="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon mr-3">
                        <i className="icon-plus3"></i>
                      </a>
                      <div>
                        <div className="font-weight-semibold">Nouveaux Visiteurs</div>
                        <span className="text-muted">2,349 avg</span>
                      </div>
                    </div>
                    <div className="w-75 mx-auto mb-3" id="new-visitors"></div>
                  </div>

                  <div className="col-sm-4">
                    <div className="d-flex align-items-center justify-content-center mb-2">
                      <a href="#" className="btn bg-transparent border-warning-400 text-warning-400 rounded-round border-2 btn-icon mr-3">
                        <i className="icon-watch2"></i>
                      </a>
                      <div>
                        <div className="font-weight-semibold">Dernière connexion</div>
                        <span className="text-muted">08:20 avg</span>
                      </div>
                    </div>
                    <div className="w-75 mx-auto mb-3" id="new-sessions">
                      
                    </div>
                  </div>

                  <div className="col-sm-4">
                    <div className="d-flex align-items-center justify-content-center mb-2">
                      <a href="#" className="btn bg-transparent border-indigo-400 text-indigo-400 rounded-round border-2 btn-icon mr-3">
                        <i className="icon-people"></i>
                      </a>
                      <div>
                        <div className="font-weight-semibold">En ligne</div>
                        <span className="text-muted"><span className="badge badge-mark border-success mr-2"></span> 5,378 avg</span>
                      </div>
                    </div>
                    <div className="w-75 mx-auto mb-3" id="total-online">
                      <div class="w-75 mx-auto mb-3" id="new-sessions"></div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="chart position-relative" id="traffic-sources">
                 <Graph />
              </div>
            </div>
            {/* <!-- /traffic sources --> */}

          </div>

          <div className="col-xl-5">

            {/* <!-- Sales stats --> */}
            <div className="card">
              <div className="card-header header-elements-inline">
                <h6 className="card-title">Statistiques</h6>
                <div className="header-elements">
                  <select className="form-control" id="select_date" data-fouc>
                    <option value="val1">June, 29 - July, 5</option>
                    <option value="val2">June, 22 - June 28</option>
                    <option value="val3" selected>June, 15 - June, 21</option>
                    <option value="val4">June, 8 - June, 14</option>
                  </select>
                </div>
              </div>

              <div className="card-body py-0">
                <div className="row text-center">
                  <div className="col-4">
                    <div className="mb-3">
                      <h5 className="font-weight-semibold mb-0">5,689</h5>
                      <span className="text-muted font-size-sm">Nouveaux anonceurs </span>
                    </div>
                  </div>

                  <div className="col-4">
                    <div className="mb-3">
                      <h5 className="font-weight-semibold mb-0">32,568</h5>
                      <span className="text-muted font-size-sm">Ce mois</span>
                    </div>
                  </div>

                  <div className="col-4">
                    <div className="mb-3">
                      <h5 className="font-weight-semibold mb-0">$23,464</h5>
                      <span className="text-muted font-size-sm">Profit</span>
                    </div>
                  </div>
                </div>
              </div>

              <div className="chart mb-2" id="app_sales">
              </div>
              <div className="chart" id="monthly-sales-stats">
                <Chart/>
              </div>
            </div>
            {/* <!-- /sales stats --> */}

          </div>
        </div>
        {/* <!-- /main charts --> */}


        {/* <!-- Dashboard content --> */}
        <div className="row">
          <div className="col-xl-8">

            {/* <!-- Marketing campaigns --> */}
            <div className="card">
              <div className="card-header header-elements-sm-inline">
                <h6 className="card-title">Anonceurs inscrits</h6>
                <div className="header-elements">
                  <span className="badge bg-success badge-pill">28 active</span>
                </div>
              </div>

              <div className="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">
                <div className="d-flex align-items-center mb-3 mb-sm-0">
                  <div id="campaigns-donut"></div>
                  <div className="ml-3">
                    <h5 className="font-weight-semibold mb-0">38,289 <span className="text-success font-size-sm font-weight-normal"><i className="icon-arrow-up12"></i> (+16.2%)</span></h5>
                    <span className="badge badge-mark border-success mr-1"></span> <span className="text-muted">May 12, 12:30 am</span>
                  </div>
                </div>

                <div className="d-flex align-items-center mb-3 mb-sm-0">
                  <div id="campaign-status-pie"></div>
                  <div className="ml-3">
                    <h5 className="font-weight-semibold mb-0">2,458 <span className="text-danger font-size-sm font-weight-normal"><i className="icon-arrow-down12"></i> (-4.9%)</span></h5>
                    <span className="badge badge-mark border-danger mr-1"></span> <span className="text-muted">Jun 4, 4:00 am</span>
                  </div>
                </div>
              </div>

              <div className="table-responsive">
                <table className="table text-nowrap">
                  <thead>
                    <tr>
                      <th>Anonceurs</th>
                      <th>Boutiques</th>
                      <th>Changements</th>
                      <th>vente</th>
                      <th>Accessibilité</th>
                      <th className="text-center" style={{width: "20px"}}><i className="icon-arrow-down12"></i></th>
                    </tr>
                    <tr className="table-active table-border-double">
                      <td colspan="5"></td>
                      <td className="text-right">
                        <span className="progress-meter" id="today-progress" data-progress="30"></span>
                      </td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div className="d-flex align-items-center">
                          <div className="mr-3">
                            <a href="#">
                              <img src="global_assets/images/brands/facebook.png" className="rounded-circle" width="32" height="32" alt="" />
                            </a>
                          </div>
                          <div>
                            <a href="#" className="text-default font-weight-semibold">Facebook</a>
                            <div className="text-muted font-size-sm">
                              <span className="badge badge-mark border-blue mr-1"></span>
                              02:00 - 03:00
                            </div>
                          </div>
                        </div>
                      </td>
                      <td><span className="text-muted">Mintlime</span></td>
                      <td><span className="text-success-600"><i className="icon-stats-growth2 mr-2"></i> 2.43%</span></td>
                      <td><h6 className="font-weight-semibold mb-0">$5,489</h6></td>
                      <td><span className="badge bg-blue">Active</span></td>
                      <td className="text-center">
                        <div className="list-icons">
                          <div className="list-icons-item dropdown">
                            <MenuDropOne/>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="d-flex align-items-center">
                          <div className="mr-3">
                            <a href="#">
                              <img src="global_assets/images/brands/youtube.png" className="rounded-circle" width="32" height="32" alt="" />
                            </a>
                          </div>
                          <div>
                            <a href="#" className="text-default font-weight-semibold">Youtube videos</a>
                            <div className="text-muted font-size-sm">
                              <span className="badge badge-mark border-danger mr-1"></span>
                              13:00 - 14:00
                            </div>
                          </div>
                        </div>
                      </td>
                      <td><span className="text-muted">CDsoft</span></td>
                      <td><span className="text-success-600"><i className="icon-stats-growth2 mr-2"></i> 3.12%</span></td>
                      <td><h6 className="font-weight-semibold mb-0">$2,592</h6></td>
                      <td><span className="badge bg-danger">Closed</span></td>
                      <td className="text-center">
                        <div className="list-icons">
                          <div className="list-icons-item dropdown">
                           <MenuDropOne/>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="d-flex align-items-center">
                          <div className="mr-3">
                            <a href="#">
                              <img src="global_assets/images/brands/spotify.png" className="rounded-circle" width="32" height="32" alt="" />
                            </a>
                          </div>
                          <div>
                            <a href="#" className="text-default font-weight-semibold">Spotify ads</a>
                            <div className="text-muted font-size-sm">
                              <span className="badge badge-mark border-grey-400 mr-1"></span>
                              10:00 - 11:00
                            </div>
                          </div>
                        </div>
                      </td>
                      <td><span className="text-muted">Diligence</span></td>
                      <td><span className="text-danger"><i className="icon-stats-decline2 mr-2"></i> - 8.02%</span></td>
                      <td><h6 className="font-weight-semibold mb-0">$1,268</h6></td>
                      <td><span className="badge bg-grey-400">On hold</span></td>
                      <td className="text-center">
                        <div className="list-icons">
                          <div className="list-icons-item dropdown">
                           <MenuDropOne/>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="d-flex align-items-center">
                          <div className="mr-3">
                            <a href="#">
                              <img src="global_assets/images/brands/twitter.png" className="rounded-circle" width="32" height="32" alt="" />
                            </a>
                          </div>
                          <div>
                            <a href="#" className="text-default font-weight-semibold">Twitter ads</a>
                            <div className="text-muted font-size-sm">
                              <span className="badge badge-mark border-grey-400 mr-1"></span>
                              04:00 - 05:00
                            </div>
                          </div>
                        </div>
                      </td>
                      <td><span className="text-muted">Deluxe</span></td>
                      <td><span className="text-success-600"><i className="icon-stats-growth2 mr-2"></i> 2.78%</span></td>
                      <td><h6 className="font-weight-semibold mb-0">$7,467</h6></td>
                      <td><span className="badge bg-grey-400">On hold</span></td>
                      <td className="text-center">
                        <div className="list-icons">
                          <div className="list-icons-item dropdown">
                            <MenuDropOne/>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="d-flex align-items-center">
                          <div className="mr-3">
                            <a href="#">
                              <img src="global_assets/images/brands/bing.png" className="rounded-circle" width="32" height="32" alt="" />
                            </a>
                          </div>
                          <div>
                            <a href="#" className="text-default font-weight-semibold">Bing campaign</a>
                            <div className="text-muted font-size-sm">
                              <span className="badge badge-mark border-success mr-1"></span>
                              15:00 - 16:00
                            </div>
                          </div>
                        </div>
                      </td>
                      <td><span className="text-muted">Metrics</span></td>
                      <td><span className="text-danger"><i className="icon-stats-decline2 mr-2"></i> - 5.78%</span></td>
                      <td><h6 className="font-weight-semibold mb-0">$970</h6></td>
                      <td><span className="badge bg-success-400">Pending</span></td>
                      <td className="text-center">
                        <div className="list-icons">
                          <div className="list-icons-item dropdown">
                            <MenuDropOne/>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="d-flex align-items-center">
                          <div className="mr-3">
                            <a href="#">
                              <img src="global_assets/images/brands/amazon.png" className="rounded-circle" width="32" height="32" alt="" />
                            </a>
                          </div>
                          <div>
                            <a href="#" className="text-default font-weight-semibold">Amazon ads</a>
                            <div className="text-muted font-size-sm">
                              <span className="badge badge-mark border-danger mr-1"></span>
                              18:00 - 19:00
                            </div>
                          </div>
                        </div>
                      </td>
                      <td><span className="text-muted">Blueish</span></td>
                      <td><span className="text-success-600"><i className="icon-stats-growth2 mr-2"></i> 6.79%</span></td>
                      <td><h6 className="font-weight-semibold mb-0">$1,540</h6></td>
                      <td><span className="badge bg-blue">Active</span></td>
                      <td className="text-center">
                        <div className="list-icons">
                          <div className="list-icons-item dropdown">
                           <MenuDropOne/>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="d-flex align-items-center">
                          <div className="mr-3">
                            <a href="#">
                              <img src="global_assets/images/brands/dribbble.png" className="rounded-circle" width="32" height="32" alt="" />
                            </a>
                          </div>
                          <div>
                            <a href="#" className="text-default font-weight-semibold">Dribbble ads</a>
                            <div className="text-muted font-size-sm">
                              <span className="badge badge-mark border-blue mr-1"></span>
                              20:00 - 21:00
                            </div>
                          </div>
                        </div>
                      </td>
                      <td><span className="text-muted">Teamable</span></td>
                      <td><span className="text-danger"><i className="icon-stats-decline2 mr-2"></i> 9.83%</span></td>
                      <td><h6 className="font-weight-semibold mb-0">$8,350</h6></td>
                      <td><span className="badge bg-danger">Closed</span></td>
                      <td className="text-center">
                        <div className="list-icons">
                          <div className="list-icons-item dropdown">
                           <MenuDropOne/>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            {/* <!-- /marketing campaigns --> */}


            {/* <!-- Quick stats boxes --> */}
            <div className="row">
              <div className="col-lg-4">

                {/* <!-- Members online --> */}
                <div className="card bg-teal-400" style={{background:'#26A69A'}}>
                  <div className="card-body">
                    <div className="d-flex">
                      <h3 className="font-weight-semibold mb-0">3,450</h3>
                      <span className="badge bg-teal-800 badge-pill align-self-center ml-auto">+53,6%</span>
                    </div>
                    
                    <div>
                      Anonceurs en ligne
                      <div className="font-size-sm opacity-75">489 avg</div>
                    </div>
                  </div>

                  <div className="container-fluid">
                    <div id="members-online"></div>
                  </div>
                </div>
                {/* <!-- /members online --> */}

              </div>

              <div className="col-lg-4">

                {/* <!-- Current server load --> */}
                <div className="card bg-pink-400" style={{background:'#EC407A'}}>
                  <div className="card-body">
                    <div className="d-flex">
                      <h3 className="font-weight-semibold mb-0">49.4%</h3>
                    </div>
                    
                    <div>
                      Charge actuelle du serveur
                      <div className="font-size-sm opacity-75">34.6% avg</div>
                    </div>
                  </div>

                  <div id="server-load"></div>
                </div>
                {/* <!-- /current server load --> */}

              </div>

              <div className="col-lg-4">

        {/* <!-- Today's revenue --> */}
        <div className="card bg-blue-400" style={{background:'#29B6F6'}}>
          <div className="card-body">
            <div className="d-flex">
              <h3 className="font-weight-semibold mb-0">$18,390</h3>
              <div className="list-icons ml-auto">
                  <a className="list-icons-item" data-action="reload"></a>
                </div>
              </div>
                    
              <div>
                Visiteurs
              <div className="font-size-sm opacity-75">$37,578 avg</div>
            </div>
          </div>

          <div id="today-revenue"></div>
        </div>
        {/* <!-- /today's revenue --> */}

      </div>
    </div>
    {/* <!-- /quick stats boxes --> */}


    {/* <!-- Support tickets --> */}
    <div className="card">
      <div className="table-responsive">
        <table className="table text-nowrap">
          <thead>
            <tr>
              <th style={{width: "50px"}}>date</th>
              <th style={{width: "300px"}}>Anonceurs</th>
              <th>Description</th>
              <th className="text-center" style={{width: "20px"}}><i className="icon-arrow-down12"></i></th>
            </tr>
          </thead>
          <tbody>
            <tr className="table-active table-border-double">
              <td colspan="3">Anonceurs sponsorisés</td>
              <td className="text-right">
                <span className="badge bg-blue badge-pill">24</span>
              </td>
            </tr>

            <tr>
              <td className="text-center">
                <h6 className="mb-0">12</h6>
                <div className="font-size-sm text-muted line-height-1">hours</div>
              </td>
              <td>
                <div className="d-flex align-items-center">
                  <div className="mr-3">
                    <a href="#" className="btn bg-teal-400 rounded-round btn-icon btn-sm">
                      <span className="letter-icon"></span>
                    </a>
                  </div>
                  <div>
                    <a href="#" className="text-default font-weight-semibold letter-icon-title">Annabelle Doney</a>
                    <div className="text-muted font-size-sm"><span className="badge badge-mark border-blue mr-1"></span> Active</div>
                  </div>
                </div>
              </td>
              <td>
                <a href="#" className="text-default">
                  <div className="font-weight-semibold">[#1183] Workaround for OS X selects printing bug</div>
                  <span className="text-muted">Chrome fixed the bug several versions ago, thus rendering this...</span>
                </a>
              </td>
              <td className="text-center">
                <div className="list-icons">
                  <div className="list-icons-item dropdown">
                    <MenuDropTwo/>
                  </div>
                </div>
              </td>
            </tr>

            <tr>
              <td className="text-center">
                <h6 className="mb-0">16</h6>
                <div className="font-size-sm text-muted line-height-1">hours</div>
              </td>
              <td>
                <div className="d-flex align-items-center">
                  <div className="mr-3">
                    <a href="#">
                      <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="32" height="32" alt="" />
                    </a>
                  </div>
                  <div>
                    <a href="#" className="text-default font-weight-semibold">Chris Macintyre</a>
                    <div className="text-muted font-size-sm"><span className="badge badge-mark border-blue mr-1"></span> Active</div>
                  </div>
                </div>
              </td>
              <td>
                <a href="#" className="text-default">
                  <div className="font-weight-semibold">[#1249] Vertically center carousel controls</div>
                  <span className="text-muted">Try any carousel control and reduce the screen width below...</span>
                </a>
              </td>
              <td className="text-center">
                <div className="list-icons">
                  <div className="list-icons-item dropdown">
                    <MenuDropTwo/>
                  </div>
                </div>
              </td>
            </tr>

            <tr>
              <td className="text-center">
                <h6 className="mb-0">20</h6>
                <div className="font-size-sm text-muted line-height-1">hours</div>
              </td>
              <td>
                <div className="d-flex align-items-center">
                  <div className="mr-3">
                    <a href="#" className="btn bg-blue rounded-round btn-icon btn-sm">
                      <span className="letter-icon"></span>
                    </a>
                  </div>
                  <div>
                    <a href="#" className="text-default font-weight-semibold letter-icon-title">Robert Hauber</a>
                    <div className="text-muted font-size-sm"><span className="badge badge-mark border-blue mr-1"></span> Active</div>
                  </div>
                </div>
              </td>
              <td>
                <a href="#" className="text-default">
                  <div className="font-weight-semibold">[#1254] Inaccurate small pagination height</div>
                  <span className="text-muted">The height of pagination elements is not consistent with...</span>
                </a>
              </td>
              <td className="text-center">
                <div className="list-icons">
                  <div className="list-icons-item dropdown">
                    <MenuDropTwo/>
                  </div>
                </div>
              </td>
            </tr>

            <tr>
              <td className="text-center">
                <h6 className="mb-0">40</h6>
                <div className="font-size-sm text-muted line-height-1">hours</div>
              </td>
              <td>
                <div className="d-flex align-items-center">
                  <div className="mr-3">
                    <a href="#" className="btn bg-warning-400 rounded-round btn-icon btn-sm">
                      <span className="letter-icon"></span>
                    </a>
                  </div>
                  <div>
                    <a href="#" className="text-default font-weight-semibold letter-icon-title">Robert Hauber</a>
                    <div className="text-muted font-size-sm"><span className="badge badge-mark border-blue mr-1"></span> Active</div>
                  </div>
                </div>
              </td>
              <td>
                <a href="#" className="text-default">
                  <div className="font-weight-semibold">[#1184] Round grid column gutter operations</div>
                  <span className="text-muted">Left rounds up, right rounds down. should keep everything...</span>
                </a>
              </td>
              <td className="text-center">
                <div className="list-icons">
                  <div className="list-icons-item dropdown">
                    <MenuDropTwo/>
                  </div>
                </div>
              </td>
            </tr>
            <tr className="table-active table-border-double">
              <td colspan="3">Anonceurs archivés</td>
              <td className="text-right">
                <span className="badge bg-danger badge-pill">37</span>
              </td>
            </tr>

            <tr>
              <td className="text-center">
                <i className="icon-cross2 text-danger-400"></i>
              </td>
              <td>
                <div className="d-flex align-items-center">
                  <div className="mr-3">
                    <a href="#">
                      <img src="global_assets/images/placeholders/placeholder.jpg" className="rounded-circle" width="32" height="32" alt="" />
                    </a>
                  </div>
                  <div>
                    <a href="#" className="text-default font-weight-semibold">Mitchell Sitkin</a>
                    <div className="text-muted font-size-sm"><span className="badge badge-mark border-danger mr-1"></span> Closed</div>
                  </div>
                </div>
              </td>
              <td>
                <a href="#" className="text-default">
                  <div>[#1040] Account for static form controls in form group</div>
                  <span className="text-muted">Resizes control label's font-size and account for the standard...</span>
                </a>
              </td>
              <td className="text-center">
                <div className="list-icons">
                  <div className="list-icons-item dropdown">
                    <MenuDropArch/>
                  </div>
                </div>
              </td>
            </tr>

            <tr>
              <td className="text-center">
                <i className="icon-cross2 text-danger"></i>
              </td>
              <td>
                <div className="d-flex align-items-center">
                  <div className="mr-3">
                    <a href="#" className="btn bg-brown-400 rounded-round btn-icon btn-sm">
                      <span className="letter-icon"></span>
                    </a>
                  </div>
                  <div>
                    <a href="#" className="text-default font-weight-semibold letter-icon-title">Katleen Jensen</a>
                    <div className="text-muted font-size-sm"><span className="badge badge-mark border-danger mr-1"></span> Closed</div>
                  </div>
                </div>
              </td>
              <td>
                <a href="#" className="text-default">
                  <div>[#1038] Proper sizing of form control feedback</div>
                  <span className="text-muted">Feedback icon sizing inside a larger/smaller form-group...</span>
                </a>
              </td>
              <td className="text-center">
                <div className="list-icons">
                  <div className="list-icons-item dropdown">
                    <MenuDropArch/>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    {/* <!-- /support tickets --> */}


  </div>

  <div className="col-xl-4">

    {/* <!-- Progress counters --> */}
    <div className="row">
      <div className="col-sm-6">

        {/* <!-- Available hours --> */}
        <div className="card text-center">
          <div className="card-body">

                    {/* <!-- Progress counter --> */}
            <div className="svg-center position-relative" id="hours-available-progress"></div>
            {/* <!-- /progress counter --> */}


            {/* <!-- Bars --> */}
            <div id="hours-available-bars"></div>
            {/* <!-- /bars --> */}

          </div>
        </div>
        {/* <!-- /available hours --> */}

      </div>

      <div className="col-sm-6">

        {/* <!-- Productivity goal --> */}
        <div className="card text-center">
          <div className="card-body">

            {/* <!-- Progress counter --> */}
            <div className="svg-center position-relative" id="goal-progress"></div>
            {/* <!-- /progress counter --> */}

            {/* <!-- Bars --> */}
            <div id="goal-bars"></div>
            {/* <!-- /bars --> */}

          </div>
        </div>
        {/* <!-- /productivity goal --> */}

      </div>
    </div>
    {/* <!-- /progress counters --> */}



    {/* <!-- My messages --> */}
    <div className="card">
      <div className="card-header header-elements-inline">
        <h6 className="card-title">Mes messages</h6>
        <div className="header-elements">
          <span><i className="icon-history text-warning mr-2"></i> Jul 7, 10:30</span>
          <span className="badge bg-success align-self-start ml-3">Online</span>
        </div>
      </div>

  
      <div className="card-body py-0">
        <div className="row text-center">
          <div className="col-4">
            <div className="mb-3">
              <h5 className="font-weight-semibold mb-0">2,345</h5>
              <span className="text-muted font-size-sm">Cette semaine</span>
            </div>
          </div>

          <div className="col-4">
            <div className="mb-3">
              <h5 className="font-weight-semibold mb-0">3,568</h5>
              <span className="text-muted font-size-sm">Ce mois</span>
            </div>
          </div>

          <div className="col-4">
            <div className="mb-3">
              <h5 className="font-weight-semibold mb-0">32,693</h5>
              <span className="text-muted font-size-sm">tous les messages</span>
            </div>
          </div>
        </div>
      </div>
    
      <div id="messages-stats"></div>
      <MessagesTabs/>
    </div>



 
    <div className="card">
      <div className="card-header header-elements-inline">
        <h6 className="card-title">Anonceurs connecté</h6>
        <div className="header-elements">
          <div className="form-check form-check-inline form-check-right form-check-switchery form-check-switchery-sm">
            <label className="form-check-label">
              <input type="checkbox" className="form-input-switchery" id="realtime" defaultChecked data-fouc />
              activé
            </label>
          </div>
          <span className="badge bg-danger-400 badge-pill">+86</span>
        </div>
      </div>

      <div className="card-body">
        <div className="chart mb-3" id="bullets"></div>

        <ul className="media-list">
          <li className="media">
            <div className="mr-3">
              <a href="#" className="btn bg-transparent border-pink text-pink rounded-round border-2 btn-icon"><i className="icon-statistics"></i></a>
            </div>
            
            <div className="media-body">
              Stats for July, 6: <span className="font-weight-semibold">1938</span> orders, <span className="font-weight-semibold text-danger">$4220</span> revenue
              <div className="text-muted">2 hours ago</div>
            </div>

            <div className="ml-3 align-self-center">
              <a href="#" className="list-icons-item"><i className="icon-more"></i></a>
            </div>
          </li>

          <li className="media">
            <div className="mr-3">
              <a href="#" className="btn bg-transparent border-success text-success rounded-round border-2 btn-icon"><i className="icon-checkmark3"></i></a>
            </div>
            
            <div className="media-body">
              Invoices <a href="#">#4732</a> and <a href="#">#4734</a> have been paid
              <div className="text-muted">Dec 18, 18:36</div>
            </div>

            <div className="ml-3 align-self-center">
              <a href="#" className="list-icons-item"><i className="icon-more"></i></a>
            </div>
          </li>

          <li className="media">
            <div className="mr-3">
              <a href="#" className="btn bg-transparent border-primary text-primary rounded-round border-2 btn-icon"><i className="icon-alignment-unalign"></i></a>
            </div>
            
            <div className="media-body">
              Affiliate commission for June has been paid
              <div className="text-muted">36 minutes ago</div>
            </div>

            <div className="ml-3 align-self-center">
              <a href="#" className="list-icons-item"><i className="icon-more"></i></a>
            </div>
          </li>

          <li className="media">
            <div className="mr-3">
              <a href="#" className="btn bg-transparent border-warning-400 text-warning-400 rounded-round border-2 btn-icon"><i className="icon-spinner11"></i></a>
            </div>

            <div className="media-body">
              Order <a href="#">#37745</a> from July, 1st has been refunded
              <div className="text-muted">4 minutes ago</div>
            </div>

            <div className="ml-3 align-self-center">
              <a href="#" className="list-icons-item"><i className="icon-more"></i></a>
            </div>
          </li>

          <li className="media">
            <div className="mr-3">
              <a href="#" className="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon"><i className="icon-redo2"></i></a>
            </div>
            
            <div className="media-body">
              Invoice <a href="#">#4769</a> has been sent to <a href="#">Robert Smith</a>
              <div className="text-muted">Dec 12, 05:46</div>
            </div>

            <div className="ml-3 align-self-center">
              <a href="#" className="list-icons-item"><i className="icon-more"></i></a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  

  </div>
</div>


</div>
  )
}
