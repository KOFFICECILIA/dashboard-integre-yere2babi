import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head>
            <meta charSet="utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
            <link href="/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css" />
            <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
            <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css" />
            <link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="/assets/css/components.min.css" rel="stylesheet" type="text/css" />
            <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css" />
        </Head>
        <body>
          <Main />
          <NextScript>
            <script src="/global_assets/js/main/jquery.min.js"></script>
            <script src="/global_assets/js/main/bootstrap.bundle.min.js"></script>
            <script src="/global_assets/js/plugins/loaders/blockui.min.js"></script>
            <script src="/global_assets/js/plugins/ui/ripple.min.js"></script>
            <script src="/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
            <script src="/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
            <script src="/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
            <script src="/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
            <script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
            <script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>

            
            <script src="/assets/js/app.js"></script>
            <script src="/global_assets/js/demo_pages/dashboard.js"></script>
          </NextScript>
        </body>
      </Html>
    )
  }
}

export default MyDocument