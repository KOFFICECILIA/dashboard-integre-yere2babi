import React from 'react'

export default function Acces_annonceurs() {
    return(
        <div className="content">
            <div className="mb-3">
                <h6 className="mb-0 font-weight-semibold">
                    Donner des accès aux anonceurs
                </h6>
                <span className="text-muted d-block">tout peut être encore modifié</span>
            </div>

            <div className="card">
                <div className="card-header header-elements-inline">
                    <h5 className="card-title">Anonceurs et accès</h5>
                    <div className="header-elements">
                        <div className="list-icons">
                            <a className="list-icons-item" data-action="collapse"></a>
                            <a className="list-icons-item" data-action="reload"></a>
                            <a className="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>

                <div className="card-body">
                    
                </div>

                <div className="table-responsive">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Nom de la boutique</th>
                                <th>Nom de l'anonceurs</th>
                                <th>Autorisation</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>KMCAD-boutique</td>
                                <td>koffi koffi</td>
                                <td><div className="row"><button className="btn-success ml-4 b-none">activer</button><button className="btn-secondary b-none ml-2">Désactiver</button> <button className="btn-danger ml-2 b-none">Suspendre</button></div></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>TTT-boutique</td>
                                <td>konan konan</td>
                                <td><div className="row"><button className="btn-success ml-4 b-none">activer</button><button className="btn-secondary b-none ml-2">Désactiver</button> <button className="btn-danger ml-2 b-none">Suspendre</button></div></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>KMCAD-boutique</td>
                                <td>koffi koffi</td>
                                <td><div className="row"><button className="btn-success ml-4 b-none">activer</button><button className="btn-secondary b-none ml-2">Désactiver</button> <button className="btn-danger ml-2 b-none">Suspendre</button></div></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>TTT-boutique</td>
                                <td>konan konan</td>
                                <td><div className="row"><button className="btn-success ml-4 b-none">activer</button><button className="btn-secondary b-none ml-2">Désactiver</button> <button className="btn-danger ml-2 b-none">Suspendre</button></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    )
}