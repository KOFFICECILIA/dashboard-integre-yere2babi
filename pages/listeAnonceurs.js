import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Link from 'next/link'

export default function Listeanonceurs() {
    return (
        
        <div>
            <div className="container">
				<div className="card mt-4">
					<div className="card-header header-elements-inline">
						<h5 className="card-title">Liste des Anonceurs</h5>
						<div className="header-elements">
							<div className="list-icons">
								<a className="list-icons-item" data-action="collapse"></a>
								<a className="list-icons-item" data-action="reload"></a>
								<a className="list-icons-item" data-action="remove"></a>
							</div>
						</div>
					</div>
	
					<table className="table datatable-basic">
						<thead>
							<tr>
								<th>Nom de la boutique</th>
								<th>Nom de l'anonceurs</th>
								<th>Job Title</th>
								<th>DOB</th>
								<th>Status</th>
								<th className="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Marth</td>
								<td><a href="#">Enright</a></td>
								<td>Traffic Court Referee</td>
								<td>22 Jun 1972</td>
								<td><span className="badge badge-success">Active</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Jackelyn</td>
								<td>Weible</td>
								<td><a href="#">Airline Transport Pilot</a></td>
								<td>3 Oct 1981</td>
								<td><span className="badge badge-secondary">Inactive</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Aura</td>
								<td>Hard</td>
								<td>Business Services Sales Representative</td>
								<td>19 Apr 1969</td>
								<td><span className="badge badge-danger">Suspended</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Nathalie</td>
								<td><a href="#">Pretty</a></td>
								<td>Drywall Stripper</td>
								<td>13 Dec 1977</td>
								<td><span className="badge badge-info">Pending</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Sharan</td>
								<td>Leland</td>
								<td>Aviation Tactical Readiness Officer</td>
								<td>30 Dec 1991</td>
								<td><span className="badge badge-secondary">Inactive</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Maxine</td>
								<td><a href="#">Woldt</a></td>
								<td><a href="#">Business Services Sales Representative</a></td>
								<td>17 Oct 1987</td>
								<td><span className="badge badge-info">Pending</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Sylvia</td>
								<td><a href="#">Mcgaughy</a></td>
								<td>Hemodialysis Technician</td>
								<td>11 Nov 1983</td>
								<td><span className="badge badge-danger">Suspended</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Lizzee</td>
								<td><a href="#">Goodlow</a></td>
								<td>Technical Services Librarian</td>
								<td>1 Nov 1961</td>
								<td><span className="badge badge-danger">Suspended</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Kennedy</td>
								<td>Haley</td>
								<td>Senior Marketing Designer</td>
								<td>18 Dec 1960</td>
								<td><span className="badge badge-success">Active</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Chantal</td>
								<td><a href="#">Nailor</a></td>
								<td>Technical Services Librarian</td>
								<td>10 Jan 1980</td>
								<td><span className="badge badge-secondary">Inactive</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Delma</td>
								<td>Bonds</td>
								<td>Lead Brand Manager</td>
								<td>21 Dec 1968</td>
								<td><span className="badge badge-info">Pending</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Roland</td>
								<td>Salmos</td>
								<td><a href="#">Senior Program Developer</a></td>
								<td>5 Jun 1986</td>
								<td><span className="badge badge-secondary">Inactive</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Coy</td>
								<td>Wollard</td>
								<td>Customer Service Operator</td>
								<td>12 Oct 1982</td>
								<td><span className="badge badge-success">Active</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Maxwell</td>
								<td>Maben</td>
								<td>Regional Representative</td>
								<td>25 Feb 1988</td>
								<td><span className="badge badge-danger">Suspended</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>Cicely</td>
								<td>Sigler</td>
								<td><a href="#">Senior Research Officer</a></td>
								<td>15 Mar 1960</td>
								<td><span className="badge badge-info">Pending</span></td>
								<td className="text-center">
									<div className="list-icons">
										<IconButton aria-label="delete">
											<DeleteIcon style={{ color: 'red', fontSize:'20' }} />
										</IconButton>
										<IconButton>
											<Link href="/modifier_annonceur">
												<EditIcon style={{ color: 'blue', fontSize:'20' }}/>
											</Link>
										</IconButton>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
        <div >
      
    </div>
</div>
    )
}